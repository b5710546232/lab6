import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.Set;
/**
 * @author Nattapat Sukpootanan
 * WordCount count allword by  using wordCounter.
 * */
public class WordCount {
	final static String DELIMS = "[\\s,.\\?!\"():;]+";
	/**main method  for count and sorted word of text Alice in wonderland by frequncy word and alphabetical order.
	 * @param agrs is not used.
	 * */
	public static void main(String[] agrs) {
		WordCounter w = new WordCounter();
		String FILE_URL = "https://bitbucket.org/skeoop/oop/raw/master/week6/Alice-in-Wonderland.txt";
		URL url = null;
		try {
			url = new URL(FILE_URL);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		InputStream input = null;
		try {
			input = url.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scanner scanner = new Scanner(input);
		scanner.useDelimiter(DELIMS);
		while(scanner.hasNext()){
			w.addWord(scanner.next());
		}
		String [] set = w.getSortedWords();
		for(int i=0;i<20;i++){
			System.out.println(set[i]+" : "+w.getCount(set[i]));
		}
		System.out.println("--------------------------------------------------");
		String[] set2 = w.getSortedNum();
		for(int i=0;i<20;i++){
		System.out.println(set2[i]+" : "+w.getCount(set2[i]));
		}
	}
}

