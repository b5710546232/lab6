import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * @author Nattapat Sukpootanan
 * WordCounter for counting word ,how many time different word occur.
 * */
public class WordCounter {
	private Map<String,Integer> allWords = new HashMap<String,Integer>();
	/**
	 * Constructor of WordCounter.
	 * */
	public WordCounter() {
	}
	/**addWord add one to the count of occurrences for this word, ignoring case.
	 * @param word for adding.
	 * */
	public void addWord(String word) {
		if(allWords.containsKey(word.toLowerCase())){
			allWords.put(word.toLowerCase(),allWords.get(word.toLowerCase())+1);
			//			hashmap.put(key, hashmap.get(key) + 1);
		}
		else
			allWords.put(word.toLowerCase(), 1);

	}
	/**
	 * Get all the words ,as a Set.
	 * @return all of words into Set.
	 * */
	public Set<String> getWords() {
		return allWords.keySet();
	}
/**
 * getCount Get the number of occurrences for a given word.
 * @param word for count.
 * @return number of occurences of given word.
 * */
	public int getCount(String word) {
		return allWords.get(word);

	}
/**
 * getSortedWords sort all word in alphabetical order.
 * @return array of String word that sorted.
 * */
	public String[] getSortedWords() {
		List<String> list = new ArrayList<String>(this.getWords());
		Collections.sort(list,String.CASE_INSENSITIVE_ORDER);
		String [] s = new String[list.size()];
		return list.toArray(s);
	}
	/**
	 * getSortedWords sort all word by frequency.
	 * @return String array of sorted all words by frequency.
	 * */
	public String[] getSortedNum(){
		String [] str = new String[allWords.size()];
		str = allWords.keySet().toArray(str);
		for(int i =0;i<str.length;i++){
			for(int j =0;j<str.length;j++){
				if(j<str.length-1&&this.getCount(str[j])-(this.getCount(str[j+1]))<0){
					String temp = str[j+1];
					str[j+1] =str[j];
					str[j] = temp;

				}
			}
		}
		return str;
	}
}
